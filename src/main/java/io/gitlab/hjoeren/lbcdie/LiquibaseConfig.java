package io.gitlab.hjoeren.lbcdie;

import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;
import liquibase.integration.cdi.CDILiquibaseConfig;
import liquibase.integration.cdi.annotations.LiquibaseType;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;

@Dependent
public class LiquibaseConfig {

  @Resource(lookup = "java:app/exampleDS")
  DataSource dataSource;

  @Produces
  @LiquibaseType
  public CDILiquibaseConfig createCDILiquibaseConfig() {
    CDILiquibaseConfig config = new CDILiquibaseConfig();
    config.setChangeLog("db.changelog.xml");
    return config;
  }

  @Produces
  @LiquibaseType
  public DataSource createDataSource() {
    return dataSource;
  }

  @Produces
  @LiquibaseType
  public ResourceAccessor createResourceAccessor() {
    return new ClassLoaderResourceAccessor(getClass().getClassLoader());
  }

}
