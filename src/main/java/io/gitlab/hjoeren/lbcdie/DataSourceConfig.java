package io.gitlab.hjoeren.lbcdie;

import javax.annotation.sql.DataSourceDefinition;

@DataSourceDefinition(
    name = "java:app/exampleDS", 
    className = "org.h2.jdbcx.JdbcDataSource",
    url = "jdbc:h2:~/example;AUTO_SERVER=TRUE",
    user = "sa",
    password = "sa"
)
public class DataSourceConfig {

}
